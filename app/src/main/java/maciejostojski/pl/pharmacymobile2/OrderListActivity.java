package maciejostojski.pl.pharmacymobile2;

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ListView;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

import maciejostojski.pl.pharmacymobile2.rest.HttpBasicUtil;
import maciejostojski.pl.pharmacymobile2.rest.object.RestAuthenticateObject;
import maciejostojski.pl.pharmacymobile2.rest.object.RestOrder;
import maciejostojski.pl.pharmacymobile2.util.adapter.OrderArrayAdapter;

public class OrderListActivity extends ListActivity {

    private final static int PICK_IMAGE  = 1;

    private RestAuthenticateObject mRestAuthenticateObject;

    private List<RestOrder> orderList = new ArrayList<>();

    private byte[] imageBytes;

    private Bitmap photoBitmap;

    private int orderId;

    @Override
    protected void onResume() {
        super.onResume();

        setContentView(R.layout.activity_order_list);

        Intent i = getIntent();
        mRestAuthenticateObject = (RestAuthenticateObject) i.getSerializableExtra("restAuthenticateObject");

        try {
            RestOrder[] responseArray = new GetOrderListTask().execute().get();
            if(responseArray != null) {
                orderList = Arrays.asList(responseArray);
                OrderArrayAdapter orderAdapter = new OrderArrayAdapter(this, R.id.dateTextView, orderList);
                setListAdapter(orderAdapter);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_order_list);
//
//        Intent i = getIntent();
//        mRestAuthenticateObject = (RestAuthenticateObject) i.getSerializableExtra("restAuthenticateObject");
//
//        try {
//            RestOrder[] responseArray = new GetOrderListTask().execute().get();
//            if(responseArray != null) {
//                orderList = Arrays.asList(responseArray);
//                OrderArrayAdapter orderAdapter = new OrderArrayAdapter(this, R.id.dateTextView, orderList);
//                setListAdapter(orderAdapter);
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        } catch (ExecutionException e) {
//            e.printStackTrace();
//        }
//    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Intent i = new Intent(this, PictureOptionChooserActivity.class);
        startActivityForResult(i, PICK_IMAGE);

        RestOrder choosenOrder = orderList.get(position);
        orderId = choosenOrder.getId();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PICK_IMAGE && resultCode == RESULT_OK) {

            if(data.getExtras() != null) {
                Bundle bundle = data.getExtras();
                Bundle cameraPhotoBundle = (Bundle) bundle.get("camera");
                photoBitmap = (Bitmap) cameraPhotoBundle.get("data");
                Log.d("d","d");
            } else if (data.getData() != null) {
                Uri uri = data.getData();
                try {
                    photoBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                    Log.d("d","d");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(photoBitmap != null) {
                new PostPhotoTask().execute();
            }
        }
    }

    private class GetOrderListTask extends AsyncTask<Void, Void, RestOrder[]> {

        private String url;

        @Override
        protected RestOrder[] doInBackground(Void... params) {

            HttpHeaders requestHeaders = HttpBasicUtil.addAuthenticateHeaders(mRestAuthenticateObject.getLogin(), mRestAuthenticateObject.getPassword());
            requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
            requestHeaders.setContentType(MediaType.APPLICATION_JSON);

            MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
            map.add("userId", String.valueOf(mRestAuthenticateObject.getUserId()));

            HttpEntity<String> httpEntity = new HttpEntity<>(map.toString(), requestHeaders);

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

            url = getString(R.string.getOrderList);
            try {
                ResponseEntity<RestOrder[]> response = restTemplate.postForEntity(url, httpEntity, RestOrder[].class);
                return response.getBody();
            } catch (HttpClientErrorException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestOrder[0];
            } catch (ResourceAccessException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestOrder[0];
            } catch(Exception e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestOrder[0];
            }
        }
    }

    private class PostPhotoTask extends AsyncTask<Void, Void, Void> {

        private String url;

        @Override
        protected Void doInBackground(Void... params) {

            HttpHeaders requestHeaders = HttpBasicUtil.addAuthenticateHeaders(mRestAuthenticateObject.getLogin(), mRestAuthenticateObject.getPassword());
            requestHeaders.setContentType(MediaType.IMAGE_JPEG);

            try {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                photoBitmap.compress(Bitmap.CompressFormat.JPEG,25, outputStream);
                imageBytes =  outputStream.toByteArray();

                HttpEntity<byte[]> httpEntity = new HttpEntity<>(imageBytes, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());
                restTemplate.getMessageConverters().add(new StringHttpMessageConverter());

                url = getString(R.string.postPhoto) + "?orderId=" + orderId;

                ResponseEntity<String> response = restTemplate.postForEntity(url, httpEntity, String.class);
                return null;
            } catch (HttpClientErrorException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return null;
            } catch (ResourceAccessException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return null;
            } catch(Exception e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return null;
            }
        }
    }
}
