package maciejostojski.pl.pharmacymobile2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class PictureOptionChooserActivity extends AppCompatActivity {

    private static final int CAMERA_ACTION = 1;

    private static final int GALLERY_ACTION = 2;

    private static final int PERMISION_CHECK  = 3;

    private static final String FILENAME = "pharmacyMobilePrescriptionTemp";

    private static final String FILEPATH = "file:///sdcard/photo.jpg";

    private Button cameraButton;

    private Button galleryButton;

    private Bitmap photoBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picture_option_chooser);

        initUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == -1) {
            switch(requestCode) {
                case CAMERA_ACTION:
                    if(data.getExtras() !=null) {
                        Bundle extras = data.getExtras();
                        photoBitmap = (Bitmap) extras.get("data");
                        Bundle bundle = new Bundle();
                        bundle.putBundle("camera", extras);
                        Intent intent = new Intent();
                        intent.putExtras(bundle);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    break;
                case GALLERY_ACTION:
                    if(data.getData() != null) {
                        Uri uri = data.getData();
                        Intent intent = new Intent();
                        intent.setData(uri);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                    break;
            }
        }
    }

    private void initUI() {
        cameraButton = (Button) findViewById(R.id.cameraButton);
        galleryButton = (Button) findViewById(R.id.galleryButton);

        setOnClickListeners();
    }

    private void setOnClickListeners() {
        setCameraButtonListener();
        setGalleryButtonListener();
    }

    private void setCameraButtonListener() {
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA},
                            PERMISION_CHECK);
                }

                Intent i = new Intent();
                i.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
                i.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);

                startActivityForResult(i, CAMERA_ACTION);
            }
        });
    }

    private void setGalleryButtonListener() {
        galleryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_GET_CONTENT);
                i.setType( "image/*" );

                startActivityForResult(i, GALLERY_ACTION);
            }
        });
    }
}
