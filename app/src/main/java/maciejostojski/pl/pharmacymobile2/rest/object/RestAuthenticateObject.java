package maciejostojski.pl.pharmacymobile2.rest.object;

import java.io.Serializable;

/**
 * Created by Maciek on 14.11.2016.
 */

public class RestAuthenticateObject implements Serializable {

    public RestAuthenticateObject() {}

    public RestAuthenticateObject(String code, Integer userId, String login, String password) {
        this.code = code;
        this.userId = userId;
        this.login = login;
        this.password = password;
    }

    private String code;

    private Integer userId;

    private String login;

    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
