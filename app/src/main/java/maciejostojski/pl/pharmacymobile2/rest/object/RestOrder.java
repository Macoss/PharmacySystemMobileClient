package maciejostojski.pl.pharmacymobile2.rest.object;

/**
 * Created by Maciek on 14.11.2016.
 */

public class RestOrder {

    private int id;

    private String date;

    private double value;

    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }
}
