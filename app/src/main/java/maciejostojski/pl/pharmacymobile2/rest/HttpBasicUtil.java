package maciejostojski.pl.pharmacymobile2.rest;

import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpHeaders;

/**
 * Created by Maciek on 14.11.2016.
 */

public class HttpBasicUtil {

    public static HttpHeaders addAuthenticateHeaders(String login, String password) {
        HttpAuthentication authHeader = new HttpBasicAuthentication(login, password);
        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setAuthorization(authHeader);
        return requestHeaders;
    }
}
