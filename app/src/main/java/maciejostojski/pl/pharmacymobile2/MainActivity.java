package maciejostojski.pl.pharmacymobile2;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.concurrent.ExecutionException;

import maciejostojski.pl.pharmacymobile2.rest.HttpBasicUtil;
import maciejostojski.pl.pharmacymobile2.rest.object.RestAuthenticateObject;

public class MainActivity extends Activity {

    private EditText loginInput;

    private EditText passwordInput;

    private Button signinButton;

    private String login;

    private String password;

    private int userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }

    private void initializeUI() {
        loginInput = (EditText) findViewById(R.id.login_edittext);
        passwordInput = (EditText) findViewById(R.id.password_edittext);
        signinButton = (Button) findViewById(R.id.signin_button);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    RestAuthenticateObject restAuthenticateObject = new AuthenticationTask().execute().get();
                    if("authenticated".equals(restAuthenticateObject.getCode())) {
                        Intent i = new Intent(MainActivity.this, OrderListActivity.class);
                        restAuthenticateObject.setLogin(login);
                        restAuthenticateObject.setPassword(password);
                        i.putExtra("restAuthenticateObject", restAuthenticateObject);
                        startActivity(i);
                    } else if ("401".equals(restAuthenticateObject.getCode())) {
                        Toast.makeText(MainActivity.this, "bad login/password", Toast.LENGTH_SHORT).show();
                    }
                    Log.i("tag", restAuthenticateObject.getCode());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private class AuthenticationTask extends AsyncTask<Void, Void, RestAuthenticateObject> {

        @Override
        protected void onPreExecute() {
            login = loginInput.getText().toString();
            password = passwordInput.getText().toString();
        }

        @Override
        protected RestAuthenticateObject doInBackground(Void... params) {
            String url = getString(R.string.authenticate);

            HttpHeaders requestHeaders = HttpBasicUtil.addAuthenticateHeaders(login, password);
            requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJacksonHttpMessageConverter());

            try {
                ResponseEntity<RestAuthenticateObject> response = restTemplate.exchange(url, HttpMethod.POST,
                        new HttpEntity<Object>(requestHeaders), RestAuthenticateObject.class);
                return response.getBody();
            } catch (HttpClientErrorException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestAuthenticateObject("401", null, null, null);
            } catch (ResourceAccessException e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestAuthenticateObject("404", null, null, null);
            } catch(Exception e) {
                Log.e("tag", e.getLocalizedMessage(), e);
                return new RestAuthenticateObject("404", null, null, null);
            }
        }
    }
}
