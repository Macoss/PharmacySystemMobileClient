package maciejostojski.pl.pharmacymobile2.util.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import maciejostojski.pl.pharmacymobile2.R;
import maciejostojski.pl.pharmacymobile2.rest.object.RestOrder;

/**
 * Created by Maciek on 16.11.2016.
 */

public class OrderArrayAdapter extends ArrayAdapter<RestOrder> {

    private Context mContext;

    private List<RestOrder> orderList;

    public OrderArrayAdapter(Context context, int resource, List<RestOrder> orderList) {
        super(context, resource, orderList);
        mContext = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        RestOrder order = orderList.get(position);

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.order_element, null);

        TextView dateTextView = (TextView) view.findViewById(R.id.dateTextView);
        TextView statusTextView = (TextView) view.findViewById(R.id.statusTextView);
        TextView valueTextView = (TextView) view.findViewById(R.id.valueTextView);

        dateTextView.setText(order.getDate());
        statusTextView.setText(order.getStatus());
        valueTextView.setText(String.valueOf(order.getValue()));

        return view;
    }
}
